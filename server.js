var http = require("http");
var express = require("express");
var WebSocket = require("ws");
var WebSocketJSONStream = require("@teamwork/websocket-json-stream");

// var redis = require("redis");
// var client = redis.createClient();

// CREATE TABLE ops (
//   collection character varying(255) not null,
//   doc_id character varying(255) not null,
//   version integer not null,
//   operation json not null, -- {v:0, create:{...}} or {v:n, op:[...]}
//   PRIMARY KEY (collection, doc_id, version)
// );
//
// CREATE TABLE snapshots (
//   collection character varying(255) not null,
//   doc_id character varying(255) not null,
//   doc_type character varying(255) not null,
//   version integer not null,
//   data json not null,
//   PRIMARY KEY (collection, doc_id)
// );

// var database = require("sharedb-postgres")({ host: "localhost", port: 5432, database: "sharedb" });
// var pubsub = require('sharedb-redis-pubsub')({ client: client });

var ShareDB = require("sharedb");

// var backend = new ShareDB({ db: database, pubsub: pubsub });
var backend = new ShareDB();
createDoc(startServer);

function createDoc(callback) {
  var connection = backend.connect();
  var doc = connection.get("issues", "one");
  doc.fetch(function(err) {
    if (err) throw err;
    if (doc.type === null) {
      doc.create({text: "Initial doc", title: "Initial title"}, callback);
      return;
    }
    callback();
  });
}

function startServer() {
  var app = express();
  app.use(express.static("static"));
  var server = http.createServer(app);

  var wss = new WebSocket.Server({server: server});
  wss.on("connection", function(ws) {
    var stream = new WebSocketJSONStream(ws);
    backend.listen(stream);
  });

  server.listen(8080);
}
