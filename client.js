var sharedb = require("sharedb/lib/client");
var StringBinding = require("sharedb-string-binding");

// remote cursor and remote selection

var ReconnectingWebSocket = require("reconnecting-websocket");

var socket = new ReconnectingWebSocket("ws://" + window.location.host);
var connection = new sharedb.Connection(socket);

var doc = connection.get("issues", "one");

doc.subscribe(function(err) {
  if (err) throw err;

  var titleElement = document.getElementById("title");
  var textElement = document.getElementById("text");

  var titleBinding = new StringBinding(titleElement, doc, ["title"]);
  var textBinding = new StringBinding(textElement, doc, ["text"]);

  titleBinding.setup();
  textBinding.setup();
});
